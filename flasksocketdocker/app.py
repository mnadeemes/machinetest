from flask import Flask
from flask import jsonify
from flask_cors import CORS, cross_origin
from flask_socketio import SocketIO, emit
from kafka import KafkaProducer, KafkaConsumer, TopicPartition
import uuid
#from engineio.payload import Payload

app = Flask(__name__)
socketio = SocketIO(app, cors_allowed_origins="*")
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'

BOOTSTRAP_SERVERS = 'host:port'
TOPIC_NAME = 'topic'

producer = KafkaProducer(bootstrap_servers=BOOTSTRAP_SERVERS)


@app.route('/')
@cross_origin()
def home():
    print('he')
    return 'Flask is running!'

@socketio.on('sockstream')
def handle_message(message):
    print('received message: ' + str(message))    
    producer.send(TOPIC_NAME, value=bytes(str(message),encoding='utf-8'), key=bytes(str(uuid.uuid4()), encoding='utf-8'))
    producer.flush()
    print('send')

if __name__ == '__main__':
    socketio.run(app, host='0.0.0.0', port=54321)
