# Remote Machine Test

## Requirement

Python + Kafka application to execute the following.

#### Task 1

1.1. Stream real-time system cursor position from system/pc to a socket

1.2. and stream from that socket to Kafka 

1.3. and then post to PostgreSQL. 

#### Task 2

2.1. Create a logic/model to detect sudden movement 

2.2. And save this to a CSV with time whenever "an event 1" occurs.

## **Flow Chart**
![FlowChart](images/flowchart.png)

## **Modules**

### **1.1 Real-time System Cursor Position Stream from System/Pc to a socket.**
#### File : cursorSocketStream.py
The cursor position get streamedto the socket as a json every 0.25sec. This was accomplished by using the following python modules:

pyautogui : to obtain real-time cursor positions.

socketio : to stream data into the socket server.


##### $ python cursorSocketStream.py

#### Stack
Python 3.6
### **1.2 Stream from socket to Kafka** 
#### File : flasksocketdocker/[app.py, docker-compose.yml, Dockerfile, requirements.txt]
Hosted a flask socket server in a docker to stream data into Kafka Topics using a Kafka Producer. Following python modules were used :

flask : host a simple web application.

flask_socketio : to create a socket stream route in the web app.

kafka : kafka producer to send stream data to kafka topics.
#### Stack
Python 3.6

Docker

Kafka Broker 0.8

$ docker-compose up --build

### **1.2 Kafka Messages post to PostgreSQL**
#### File : kafkaSpark.py
Kafka Messages can be exported/posted into PostgreSQL in multiple means such as Kafka Connect JDBC Sink or by running a spark streaming application to read messages from Kafka Topics and then save into POstgres using a JDBC driver. This application used spark streaming to connect and save messages to PostgreSQL database. The following modules where used to run the application using spark-submit :

pyspark : to create spark context

pyspark-streaming : to create and run spark streaming

pyspark.streaming.kafka : to obtain messages DStream from kafka 

pyspark.sql : to create dataframes from DStream Rdds and then write them into Postgres.

$ spark-submit --master yarn --conf spark.ui.port=12890 --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.3.2,org.postgresql:postgresql:42.2.2 --jars=postgresql-42.2.2.jar kafkaSpark.py
#### Stack
Spark 2.3.2

Spark-Streaming-Kafka 0-8_2.11:2.3.2

PostgreSQL 42.2.2
### **2.1 Logic to detect sudden movement**
Movements can easily be detected using real-time cursor positions. Here, the application can detect movement in every second. The incoming positions in each second was grouped into a set such that if there were cursor movements, the resultant set will contain more than 1 elements,else if there was no cursor movements, only the static position of cursor. Filtering the set with elements equal to 1 will give out the set with the positions moved.
### **2.2 Save this to a CSV with time whenever an event 2.1 occurs**
#### File : kafkaSpark.py
The model to detect event 2.1 returns a dstream with a set of cursor positions. It's futher transformed into dataframes to save the positions as csv.

$ spark-submit --master yarn --conf spark.ui.port=12890 --packages org.apache.spark:spark-streaming-kafka-0-8_2.11:2.3.2,org.postgresql:postgresql:42.2.2 --jars=postgresql-42.2.2.jar kafkaSpark.py

## **Test Module**
### File hdfsspark.py
To read the csv file from HDFS. The CSV file should only contain events with more than one cursor position, ie a cursor movement have been detected.

$ spark-submit --master yarn --conf spark.ui.port=12890 hdfsspark.py

## **Screenshots**
### Stream real-time system Console
![SocketClient](images/SocketClient.png)
### Stream saved in Postgres
![SavedInPSQL](images/SavedInPSQL.png)
### Detecting cursor movement 
![SparkStreamingConsole](images/SparkStreamingConsole.png)
### Events saved as CSV
![SavedAsCSV](images/SavedAsCSV.png)

