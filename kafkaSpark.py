from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils
from pyspark.sql.types import *
from pyspark.sql import SparkSession,SQLContext
import pyspark.sql.functions as F
import pyspark.sql.types as T
import json

def createContext():
    sc=SparkContext(appName="PythonSparkKafka")
    sc.setLogLevel("ERROR")
    ssc=StreamingContext(sc, 3)
    sqlContext = SQLContext(sc)
    topic="topic"
    directKafkaStream = KafkaUtils.createDirectStream(ssc, [topic],{"bootstrap.servers": 'host:port'})
    filteredStream = directKafkaStream.filter(lambda x : len(x) > 0)
    jsonStream = filteredStream.map(lambda x: json.loads(x[1].replace("\'", "\"")))

    def saveToPostgreSQL(rdd):
        #schema =  StructType([StructField ("event_json", StringType(), True)])
        df = sqlContext.createDataFrame(rdd)
        properties = {"user": "postgres","password": "password","stringtype":"unspecified","driver": "org.postgresql.Driver"}
        df.write.mode("append").jdbc(url='jdbc:postgresql://host:5432/', table="zmousecheck", properties=properties)
        df.show()

    saved = jsonStream.foreachRDD(saveToPostgreSQL)
    positionsStream = jsonStream.map(lambda x: (x['ts'].split('.')[0],str(x['x'])+'x'+str(x['y'])))
    groupedPositionsStream = positionsStream.groupByKey().map(lambda x: (x[0], list(set(x[1])))).map(lambda x : (x[0],(x[1],len(x[1]))))
    sortedGroupedPositionsStream = groupedPositionsStream.transform( lambda rdd: rdd.sortBy(lambda x: x[0], ascending=True))

    def testrdd(rdd):
            schema =  StructType([StructField ("ts", StringType(), True) , StructField("movements" , StringType(), True)])
            df = sqlContext.createDataFrame(rdd,schema)
            df.write.csv(path='/path/to/csvfile/filename.csv/',mode='append',sep=',')

    eventStream = sortedGroupedPositionsStream.filter(lambda x : x[1][1] > 1).map(lambda x : (x[0],'--'.join(x[1][0])))
    eventStream.foreachRDD(testrdd)
    eventStream.pprint()
    return ssc

#ssc = StreamingContext.getOrCreate('/tmp/checkpoint_v01',lambda: createContext())	
if __name__ == '__main__':
    ssc = createContext()
    ssc.sparkContext.setLogLevel("ERROR")
    ssc.start()
    ssc.awaitTermination()