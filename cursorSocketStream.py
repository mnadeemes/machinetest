import socketio
import pyautogui as pm
import time, datetime, sys


def main():

    sio = socketio.Client()
    sio.connect('socketserverhost:54321/')
    print("Starting cursor stream to socket3")
    #, namespaces=['/kafka']
    while True:
            pos = pm.position()
            msg = {'ts':str(time.time()),'x':pos[0],'y':pos[1]}
            print(msg)
            sio.emit('sockstream',msg)
            time.sleep(0.25)
    sio.wait()
if __name__ == '__main__':
    main()
