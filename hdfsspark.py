from pyspark import SparkContext, SparkConf
from pyspark.sql import Row,SparkSession

conf = SparkConf().setAppName("myFirstApp")
sc = SparkContext(conf=conf)
park = SparkSession(sc)
def g(x):
	print(x)

textFile = sc.textFile('/path/to/csvfile/filename.csv/')
textFile = textFile.map(lambda o: Row(o.split(",")[0],o.split(",")[1])).toDF("ts", "movements")
textFile.show(truncate=False)
